/* 
   1. Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
    - Це спосіб, за допомогою якого об'єкти можуть успадковувати властивості і методи інших об'єктів, використовуючи їхні прототипи
   2. Для чого потрібно викликати super() у конструкторі класу-нащадка?
    - ключове слово super використовується для виклику конструктора батьківського класу з конструктора класу-нащадка.
 */


class Employee {
   constructor(name, age, salary) {
    this.name = name;
    this.age = age;
    this.salary = salary;
   }

   get Name() {
    return this.name;
   }

   set Name(newName) {
    this.name = newName;
   }

   get Age() {
    return this.age;
   }

   set Age(newAge) {
    this.age = newAge;
   }

   get Salary() {
    return this.salary;
   }

   set Salary(newSalary) {
    this.salary = newSalary;
   }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
      this.lang = lang;
  }

  get Salary() {
    return `New salary: ${this.newSalary()}`;
  }

  newSalary() {
    return this.salary * 3;
  }
}

const vital = new Programmer('Vital', 35, 500, 'JS');
console.log(vital);
console.log(vital.Salary);

const bob = new Programmer('Bob', 25, 1000, 'PHP');
console.log(bob);
console.log(bob.Salary);

const anna = new Programmer('Anna', 27, 1500, 'Python');
console.log(anna);
console.log(anna.Salary);

















